'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Database = use('Database') 
const Campus = use('App/Models/Campus')

/**Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})**/
//Route.get('/category/all','CategoryController.index')

//Route.get('/campuses/all','CampusController.index')
//Route.get('/items/all','ItemController.index')

/**Route.get('/campuses/all', async () =>{
  return await Campus.all()
})**/

Route.resource('career', 'CareerController')
.apiOnly()

Route.resource('universities', 'UniversitiesController')
.apiOnly()

Route.resource('category','CategoryController')
.apiOnly()
 
Route.resource('campuses','CampusController')
.apiOnly()

Route.resource('userdetail', 'UserDetailController')
.apiOnly()

Route.resource('item','ItemController')
.apiOnly()

Route.resource('campusitem', 'CampusItemController')
.apiOnly()

Route.resource('categoryitem', 'CategoryItemController')
.apiOnly()

Route.resource('user', 'UserController')
.apiOnly()

Route.resource('dogs', 'DogController').apiOnly()


Route.resource('persons', 'PersonController')
.apiOnly()
