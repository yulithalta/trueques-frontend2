'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategoryItemSchema extends Schema {
  up () {
    this.create('category_item', (table) => {
      table.integer("item_id").notNullable().unsigned().references("id").inTable("items")
      table.integer("category_id").notNullable().unsigned().references("id").inTable("categories")
      table.timestamps()
    })
  }

  down () {
    this.drop('category_item')
  }
}

module.exports = CategoryItemSchema
