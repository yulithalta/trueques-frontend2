'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CampusesSchema extends Schema {
  up () {
    this.create('campuses', (table) => {
      table.increments()
      table.integer("university_id").notNullable().unsigned().references("id").inTable("universities")
      table.string("name",150).notNullable()
      table.string("address").notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('campuses')
  }
}

module.exports = CampusesSchema
