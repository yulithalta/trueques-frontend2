'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserDetailsSchema extends Schema {
  up () {
    this.create('user_details', (table) => {
      table.increments("id")
      table.integer("id_user").notNullable().unsigned().references("id").inTable("users")
      table.integer("id_career").notNullable().unsigned().references("id").inTable("careers")
      table.integer("id_campus").notNullable().unsigned().references("id").inTable("campuses")
      table.string("name",100).notNullable()
      table.string("last",50).notNullable()
      table.binary("image")
      table.string("email",50).notNullable().unique()
      table.string("password",50).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('user_details')
  }
}

module.exports = UserDetailsSchema
