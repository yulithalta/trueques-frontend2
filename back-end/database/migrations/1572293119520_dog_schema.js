'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DogSchema extends Schema {
  up () {
    this.create('dogs', (table) => {
      table.increments()
      table.timestamps()
      table.string('name', 20).notNullable()
      table.integer('owner').unsigned().references('id').inTable('users')
      table.string('breed', 30)
      table.integer('age').unsigned()
    })
  }

  down () {
    this.drop('dogs')
  }
}

module.exports = DogSchema
