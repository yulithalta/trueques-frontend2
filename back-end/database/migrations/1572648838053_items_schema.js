'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ItemsSchema extends Schema {
  up () {
    this.create('items', (table) => {
      table.increments()
      table.integer("user_id").notNullable().unsigned().references("id").inTable("users")
      table.string("name",150).notNullable()
      table.string("desc").notNullable()
      table.binary("image")
      table.string("wanted").notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('items')
  }
}

module.exports = ItemsSchema
