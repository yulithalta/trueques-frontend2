'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CampusItemSchema extends Schema {
  up () {
    this.create('campus_item', (table) => {
      table.integer("item_id").notNullable().unsigned().references("id").inTable("items")
      table.integer("campus_id").notNullable().unsigned().references("id").inTable("campuses")
      table.timestamps()
    })
  }

  down () {
    this.drop('campus_item')
  }
}

module.exports = CampusItemSchema
