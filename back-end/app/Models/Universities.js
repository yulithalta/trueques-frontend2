'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Universities extends Model {
    static get table (){
        return 'universities'
    }

    /**static get foreignKey () {
        return 'id_university'
      }**/

    campus() {
        return this.hasOne("App/Models/Campus")
    }
}

module.exports = Universities
