'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CampusItem extends Model {
    campuses(){
        return this.belongsToMany("App/Models/Campus")
    }

    categories(){
        return this.belongsToMany("App/Models/Category")
    }
}

module.exports = CampusItem
