'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Campus extends Model {
    static get table (){
        return 'campuses'
    }

    university(){
        return this.belongsTo("App/Models/University")
    }

    items(){
        return this.belongsToMany('App/Models/Item')
    }
}

module.exports = Campus
