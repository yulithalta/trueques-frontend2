'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const CategoryItem = use('App/Models/CategoryItem')


/**
 * Resourceful controller for interacting with categoryitems
 */
class CategoryItemController {
  /**
   * Show a list of all categoryitems.
   * GET categoryitems
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    return await CategoryItem.all()
  }

  /**
   * Create/save a new categoryitem.
   * POST categoryitems
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
      const categoryitem = new CategoryItem()
      const req = JSON.parse(request.post().json)  //convirtiendo json a objeto
  
      categoryitem.item_id = req.item_id
      categoryitem.category_id= req.category_id

      await categoryitem.save()
      return response.created(categoryitem) 
  }

  /**
   * Display a single categoryitem.
   * GET categoryitems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const categoryitem = await CategoryItem.find(params.id)
        if(categoryitem == null){
          return 'The category item does not exist'
        }
        return categoryitem
  }


  /**
   * Update categoryitem details.
   * PUT or PATCH categoryitems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
      const categoryitem = await CategoryItem.find(params.id) 
      if(categoryitem == null){
         return 'The category item does not exist'
      }
        
      const req = JSON.parse(request.post().json)  
      categoryitem.item_id = req.item_id
      categoryitem.category_id= req.category_id
    
      await categoryitem.save() 
      return 'Category item Modified'
  }

  /**
   * Delete a categoryitem with id.
   * DELETE categoryitems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const categoryitem= await CategoryItem.find(params.id)
        if(categoryitem == null){
          return 'The Category Item does not exist'
        }
        await categoryitem.delete()
        return 'Category Item deleted'
      }
  }

module.exports = CategoryItemController
