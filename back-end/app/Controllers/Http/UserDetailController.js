'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const UserDetail = use('App/Models/UserDetail')

/**
 * Resourceful controller for interacting with userdetails
 */
class UserDetailController {
  /**
   * Show a list of all userdetails.
   * GET userdetails
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    return await UserDetail.all()

  }

  /**
   * Render a form to be used for creating a new userdetail.
   * GET userdetails/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new userdetail.
   * POST userdetails
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
      const userdetail = new UserDetail()
      const req = JSON.parse(request.post().json)  //convirtiendo json a objeto
  
      userdetail.id_user = req.id_user
      userdetail.id_career = req.id_career
      userdetail.id_campus = req.id_campus
      userdetail.name = req.name
      userdetail.last = req.last
      userdetail.image = req.image
      userdetail.email = req.email
      userdetail.password = req.password

      await userdetail.save()
      return response.created(userdetail)
  }

  /**
   * Display a single userdetail.
   * GET userdetails/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const userdetail = await UserDetail.find(params.id)
    if(userdetail == null){
      return 'The user details does not exist'
    }
    return userdetail
  }

  /**
   * Render a form to update an existing userdetail.
   * GET userdetails/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update userdetail details.
   * PUT or PATCH userdetails/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const userdetail = await UserDetail.find(params.id) 
        if(userdetail == null){
          return 'The campus does not exist'
        }
        
        const req = JSON.parse(request.post().json)  
        userdetail.id_user = req.id_user
        userdetail.id_career = req.id_career
        userdetail.id_campus = req.id_campus
        userdetail.name = req.name
        userdetail.last = req.last
        userdetail.image = req.image
        userdetail.email = req.email
        userdetail.password = req.password
    
        await userdetail.save() 
        return 'user details Modified'
  }

  /**
   * Delete a userdetail with id.
   * DELETE userdetails/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const userdetail = await UserDetail.find(params.id)
        if(userdetail == null){
          return 'The User Details does not exist'
        }
        await userdetail.delete()
        return 'User Details deleted'
  }
}

module.exports = UserDetailController
