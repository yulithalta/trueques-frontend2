'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const CampusItem = use('App/Models/CampusItem')

/**
 * Resourceful controller for interacting with campusitems
 */
class CampusItemController {
  /**
   * Show a list of all campusitems.
   * GET campusitems
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    return await CampusItem.all()

  }


  /**
   * Create/save a new campusitem.
   * POST campusitems
   *
   * @param {object} ctx
   * @param {Request} ctx.reqhuest
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const campusitem = new CampusItem()
      const req = JSON.parse(request.post().json)  //convirtiendo json a objeto
  
      campusitem.item_id = req.item_id
      campusitem.campus_id = req.campus_id

      await campusitem.save()
      return response.created(campusitem)
  }

  /**
   * Display a single campusitem.
   * GET campusitems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
      const campusitem = await CampusItem.find(params.id)
        if(campusitem == null){
          return 'The campus item does not exist'
        }
        return campusitem
  }


  /**
   * Update campusitem details.
   * PUT or PATCH campusitems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const campusitem = await CampusItem.find(params.id) 
    if(campusitem == null){
      return 'The campus item does not exist'
    }
    
    const req = JSON.parse(request.post().json)  
    campusitem.item_id = req.item_id
    campusitem.campus_id = req.campus_id

    await campusitem.save() 
    return 'Campus item Modified'
  }

  /**
   * Delete a campusitem with id.
   * DELETE campusitems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
      const campusitem= await CampusItem.find(params.id)
        if(campusItem == null){
          return 'The Campus Item does not exist'
        }
        await campusitem.delete()
        return 'Campus Item deleted'
      }
  }

module.exports = CampusItemController
