'use strict'

const Campus = use('App/Models/Campus')

class CampusController {
    async index(){
        return await Campus.query().with('universities').fetch()
        //return await Campus.all()
    }
    async store ({ request, response }) {
      const campus = new Campus()
      const req = JSON.parse(request.post().json)  //convirtiendo json a objeto
  
      campus.university_id = req.university_id
      campus.name = req.name
      campus.address = req.address

      await campus.save()
      return response.created(campus)
    }
    
    async show ({ params, request, response, view }) {
        const campus = await Campus.find(params.id)
        if(campus == null){
          return 'The campus does not exist'
        }
        return campus
    }

    async update ({ params, request, response }) {
        const campus = await Campus.find(params.id) 
        if(campus == null){
          return 'The campus does not exist'
        }
        
        const req = JSON.parse(request.post().json)  
        campus.university_id = req.university_id
        campus.name = req.name
        campus.address = req.address
    
        await campus.save() 
        return 'Campus Modified'
    }
    
    async destroy ({ params, request, response }) {
        const campus= await Campus.find(params.id)
        if(campus == null){
          return 'The campus does not exist'
        }
        await campus.delete()
        return 'campus deleted'
      }
}

module.exports = CampusController
