'use strict'

const Universities = use('App/Models/Universities')

class UniversitiesController {
    async index ({ request, response, view }) {
        return await Universities.all()
        //const Career=use('App/Models/UserDetail')
        //return await Career.query().with('items').fetch()
      }
    
      async store ({ request, response }) {
        //const req = request.all()
        const universities = new Universities()
        const req = JSON.parse(request.post().json)  //convirtiendo json a objeto
    
        universities.name = req.name
    
        await universities.save()
        return response.created(universities)
      }

      async show ({ params, request, response, view }) {
        const universities = await Universities.find(params.id)
        if(universities == null){
          return 'The universities does not exist'
        }
        return universities
      }       
    
      async update ({ params, request, response }) {
        const universities = await Universities.find(params.id) 
        if(universities == null){
          return 'The universities does not exist'
        }
        
        const req = JSON.parse(request.post().json)  
        universities.name = req.name
    
        await universities.save() 
        return 'universities modified'
      }
            
      async destroy ({ params, request, response }) {
        const universities = await Universities.find(params.id)
        if(universities == null){
          return 'The universities does not exist'
        }
        await universities.delete()
        return 'universities deleted'
      }
}

module.exports = UniversitiesController
