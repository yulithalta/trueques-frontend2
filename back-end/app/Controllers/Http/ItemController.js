'use strict'

const Item = use('App/Models/Item')


class ItemController {
    async index(){
        return await Item.query().with('categories').fetch()
        //return await Item.all()

    }
    
    async store ({ request, response }) {
      const item = new Item()
      const req = JSON.parse(request.post().json)  
  
      item.user_id = req.user_id
      item.name = req.name
      item.desc = req.desc
      item.image = req.image
      item.wanted = req.wanted

      await item.save()
      return response.created(item)
      }
    
      async show ({ params, request, response, view }) {
        const item = await Item.find(params.id)
        if(item == null){
          return 'The item does not exist'
        }
        return item
      }
    
      async update ({ params, request, response }) {
        const item = await Item.find(params.id) 
        if(item == null){
          return 'The item does not exist'
        }
        
        const req = JSON.parse(request.post().json)  
        item.user_id = req.user_id
        item.name = req.name
        item.desc = req.desc
        item.image = req.image
        item.wanted = req.wanted
    
        await item.save() 
        return 'item modified'
      }
    
      async destroy ({ params, request, response }) {
        const item = await Item.find(params.id)
        if(item == null){
          return 'The category does not exist'
        }
        await item.delete()
        return 'item deleted'
      }
    
}

module.exports = ItemController
