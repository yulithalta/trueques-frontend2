'use strict'
const Career = use('App/Models/Career')

class CareerController {
    async index ({ request, response, view }) {
        return await Career.all()
        //const Career=use('App/Models/UserDetail')
        //return await Career.query().with('items').fetch()
      }
    
      async store ({ request, response }) {
        //const req = request.all()
        const career = new Career()
        const req = JSON.parse(request.post().json)  //convirtiendo json a objeto
    
        career.name = req.name
    
        await career.save()
        return response.created(career)
      }

      async show ({ params, request, response, view }) {
        const career = await Career.find(params.id)
        if(career == null){
          return 'The career does not exist'
        }
        return career
      }       
    
      async update ({ params, request, response }) {
        const career = await Career.find(params.id) 
        if(career == null){
          return 'The career does not exist'
        }
        
        const req = JSON.parse(request.post().json)  
        career.name = req.name
    
        await career.save() 
        return 'career modified'
      }
            
      async destroy ({ params, request, response }) {
        const career = await Career.find(params.id)
        if(career == null){
          return 'The career does not exist'
        }
        await career.delete()
        return 'career deleted'
      }
}

module.exports = CareerController
