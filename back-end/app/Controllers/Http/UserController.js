'use strict'

const User = use('App/Models/User')

class UserController {
    async index(){
        return await User.all()
    }
    async store ({ request, response }) {
      const user = new User()
      const req = JSON.parse(request.post().json)  //convirtiendo json a objeto
  
      user.username = req.username
      user.email = req.email
      user.password= req.password

      await user.save()
      return response.created(user)
    }

    async show ({ params, request, response, view }) {
        const user = await User.find(params.id)
        if(user == null){
          return 'The user does not exist'
        }
        return user
    }

    async update ({ params, request, response }) {
        const user = await User.find(params.id) 
        if(user == null){
          return 'The user does not exist'
        }
        
        const req = JSON.parse(request.post().json)  
        user.username = req.username
        user.email = req.email
        user.password= req.password
    
        await user.save() 
        return 'User Modified'
    }
    
    async destroy ({ params, request, response }) {
        const user = await User.find(params.id)
        if(user == null){
          return 'The campus does not exist'
        }
        await user.delete()
        return 'User deleted'
      }
}

module.exports = UserController
