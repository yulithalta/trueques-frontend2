'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Dog = use('App/Models/Dog')

/**
 * Resourceful controller for interacting with dogs
 */
class DogController {
  /**
   * Show a list of all dogs.
   * GET dogs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    return await Dog.all()
  }

  /**
   * Create/save a new dog.
   * POST dogs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  async store ({ request, response }) {
    //const req = JSON.parse(request.post().json)  //para que funcione en postman
    const req = request.post() //para que funcione en el front
    const dog = new Dog()

    dog.name = req.name
    dog.breed = req.breed
    dog.age = req.age

    await dog.save()
    return response.created()
  }

  /**
   * Display a single dog.
   * GET dogs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const dog = await Dog.find(params.id)
    if(dog == null){
      return 'The dog does not exist'
    }
    return dog
  }

  /**
   * Update dog details.
   * PUT or PATCH dogs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  async update ({ params, request, response }) {
    //find the the dog to update by id
    const dog = await Dog.find(params.id)
    if(dog == null){
      return response.notFound()
    }
    //const req = JSON.parse(request.post().json)  //para postman
    const req = request.post() // para frontend
    dog.name = req.name
    dog.breed = req.breed
    dog.age = req.age
    await dog.save() 
    return response.created()
  }

  /**
   * Delete a dog with id.
   * DELETE dogs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  
  async destroy ({ params, request, response }) {
    //find the the dog to delete by id
    const dog = await Dog.find(params.id)
    if(dog == null){
      return response.notFound()
    }
    await dog.delete()
    return response.created()
  }

}

module.exports = DogController
