'use strict'
const Category=use('App/Models/Category')

class CategoryController {
    async index(){
        //return await Category.query().with('items').fetch()
        return await Category.all()
    }
    async store ({ request, response }) {
      const category = new Category()
      const req = JSON.parse(request.post().json)  //convirtiendo json a objeto
  
      category.name = req.name
  
      await category.save()
      return response.created(category)
      }
    
      async show ({ params, request, response, view }) {
        const category = await Category.find(params.id)
        if(category == null){
          return 'The category does not exist'
        }
        return category
      }
    
      async update ({ params, request, response }) {
        const category = await Category.find(params.id) 
        if(category == null){
          return 'The category does not exist'
        }
        
        const req = JSON.parse(request.post().json)  
        category.name = req.name
    
        await category.save() 
        return 'category modified'
      }
    
      async destroy ({ params, request, response }) {
        const category = await Category.find(params.id)
        if(category == null){
          return 'The category does not exist'
        }
        await category.delete()
        return 'category deleted'
      }
}

module.exports = CategoryController
