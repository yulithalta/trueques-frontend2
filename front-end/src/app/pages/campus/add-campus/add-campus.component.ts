import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/services/toast-service';
import { CampusService } from 'src/app/services/campus.service';

@Component({
  selector: 'app-add-campus',
  templateUrl: './add-campus.component.html',
  styleUrls: ['./add-campus.component.scss']
})
export class AddCampusComponent implements OnInit {

  private campus = {}

  constructor(
    private toastCtrl: ToastService,
    private campusService: CampusService,
    private location: Location
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.campusService.saveCampus(this.campus).subscribe(
      () => {
        this.toastCtrl.presentToast('Has Agregado una Campus!');
        this.location.back();
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Ha Ocurrido un Error :(');
      }
    )
  }
}