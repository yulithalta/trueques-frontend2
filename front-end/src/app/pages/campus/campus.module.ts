import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../components/shared.module';

import { CampusComponent } from './campus.component';
import { AddCampusComponent } from './add-campus/add-campus.component';
import { UpdateCampusComponent } from './update-campus/update-campus.component';


@NgModule({
  declarations: [CampusComponent, AddCampusComponent, UpdateCampusComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: CampusComponent
      },
      {
        path: 'agregar-campus',
        component: AddCampusComponent
      },
      {
        path: 'modificar-campus',
        component: UpdateCampusComponent
      }
    ])
  ]
})
export class CampusModule { }
