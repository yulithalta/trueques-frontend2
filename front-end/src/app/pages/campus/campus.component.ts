import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CampusService } from '../../services/campus.service';

import { ToastService } from 'src/app/services/toast-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-campus',
  templateUrl: './campus.component.html',
  styleUrls: ['./campus.component.scss']
})
export class CampusComponent implements OnInit {
  public campus;
  
  constructor(
    private campusService: CampusService,
    private toastCtrl: ToastService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getCampus();
  }

  // events
  onItemClick(params): void {
    let campus = JSON.stringify(params)
    this.toastCtrl.presentToast('onItemClick: '+ campus);
  }

  
  getCampus(){
    this.campusService.getCampus().subscribe(campus => {
      this.campus = campus
    })
  }
  
  onDelete(campus){
    this.campusService.deleteCampus(campus.id).subscribe(
      () => {
        this.toastCtrl.presentToast('Se ha eliminado la campus ' + campus.title);
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Error: ' + error.statusText);
      }
    );
  }

  //save the dog to update in localStorage, then is retrieved in the update-dog.component.ts
  onUpdate(campus){
    localStorage.setItem("campus", JSON.stringify(campus));
    this.router.navigate(['/campus/modificar-campus']);
  }
  
}
