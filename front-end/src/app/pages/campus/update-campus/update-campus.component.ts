import { Component, OnInit} from '@angular/core';
import { CampusService} from '../../../services/campus.service';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/services/toast-service';
@Component({
  selector: 'app-update-campus',
  templateUrl: './update-campus.component.html',
  styleUrls: ['./update-campus.component.scss']
})
export class UpdateCampusComponent implements OnInit {
  public campusReference;
  public campus = {
    "university_id":'',
    "name": '',
    "address": '',
  }
  constructor(
    private campusService: CampusService,
    private toastCtrl: ToastService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.retrieveCampus()
  }

  retrieveCampus(){
    this.campusReference = JSON.parse(localStorage.getItem("campus"));

  }

  onUpdate(){
    this.campus.name = this.campusReference.title
    console.log(this.campus)
    this.campusService.updateCampus(this.campusReference.id, this.campus).subscribe(
      () => {
        this.toastCtrl.presentToast('Has Modificado la campus!');
        this.location.back();
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Ha Ocurrido un Error :(');
      }
    )
  }

}
