import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/services/toast-service';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  private category = {}

  constructor(
    private toastCtrl: ToastService,
    private categoryService: CategoryService,
    private location: Location
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.categoryService.saveCategory(this.category).subscribe(
      () => {
        this.toastCtrl.presentToast('Has Agregado una Categoria!');
        this.location.back();
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Ha Ocurrido un Error :(');
      }
    )
  }
}