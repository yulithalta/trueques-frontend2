import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CategoryService } from '../../services/category.service';

import { ToastService } from 'src/app/services/toast-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  public category;
  
  constructor(
    private categoryService: CategoryService,
    private toastCtrl: ToastService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getCategory();
  }

  // events
  onItemClick(params): void {
    let category = JSON.stringify(params)
    this.toastCtrl.presentToast('onItemClick: '+ category);
  }

  
  getCategory(){
    this.categoryService.getCategory().subscribe(category => {
      this.category= category
    })
  }
  
  onDelete(category){
    this.categoryService.deleteCategory(category.id).subscribe(
      () => {
        this.toastCtrl.presentToast('Se ha eliminado la categoria ' + category.title);
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Error: ' + error.statusText);
      }
    );
  }

  //save the dog to update in localStorage, then is retrieved in the update-dog.component.ts
  onUpdate(category){
    localStorage.setItem("category", JSON.stringify(category));
    this.router.navigate(['/category/modificar-carrera']);
  }
  
}
