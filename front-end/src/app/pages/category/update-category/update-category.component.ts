import { Component, OnInit} from '@angular/core';
import { CategoryService} from '../../../services/category.service';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/services/toast-service';
@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.scss']
})
export class UpdateCategoryComponent implements OnInit {
  public categoryReference;
  public category = {
    "name": '',
  }
  constructor(
    private categoryService: CategoryService,
    private toastCtrl: ToastService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.retrieveCategory()
  }

  retrieveCategory(){
    this.categoryReference = JSON.parse(localStorage.getItem("categoryr"));
    this.categoryReference.title = this.categoryReference.title.split(": ")
    this.categoryReference.title = this.categoryReference.title[1]
    console.log("el category: " + this.categoryReference);
  }

  onUpdate(){
    this.category.name = this.categoryReference.title
    console.log(this.category)
    this.categoryService.updateCategory(this.categoryReference.id, this.category).subscribe(
      () => {
        this.toastCtrl.presentToast('Has Modificado la categoria!');
        this.location.back();
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Ha Ocurrido un Error :(');
      }
    )
  }

}
