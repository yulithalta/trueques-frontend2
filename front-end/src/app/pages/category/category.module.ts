import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../components/shared.module';

import { CategoryComponent } from './category.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { UpdateCategoryComponent } from './update-category/update-category.component';


@NgModule({
  declarations: [CategoryComponent, AddCategoryComponent, UpdateCategoryComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: CategoryComponent
      },
      {
        path: 'agregar-categoria',
        component: AddCategoryComponent
      },
      {
        path: 'modificar-categoria',
        component: UpdateCategoryComponent
      }
    ])
  ]
})
export class CategoryModule { }
