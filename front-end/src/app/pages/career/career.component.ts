import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CareerService } from '../../services/career.service';

import { ToastService } from 'src/app/services/toast-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-career',
  templateUrl: './career.component.html',
  styleUrls: ['./career.component.scss']
})
export class CareerComponent implements OnInit {
  public career;
  
  constructor(
    private careerService: CareerService,
    private toastCtrl: ToastService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getCareer();
  }

  // events
  onItemClick(params): void {
    let career = JSON.stringify(params)
    this.toastCtrl.presentToast('onItemClick: '+ career);
  }

  
  getCareer(){
    this.careerService.getCareer().subscribe(career => {
      this.career = career
    })
  }
  
  onDelete(career){
    this.careerService.deleteCareer(career.id).subscribe(
      () => {
        this.toastCtrl.presentToast('Se ha eliminado la careera ' + career.title);
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Error: ' + error.statusText);
      }
    );
  }

  //save the dog to update in localStorage, then is retrieved in the update-dog.component.ts
  onUpdate(career){
    localStorage.setItem("career", JSON.stringify(career));
    this.router.navigate(['/career/modificar-carrera']);
  }
  
}
