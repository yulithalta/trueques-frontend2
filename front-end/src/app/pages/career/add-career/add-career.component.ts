import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/services/toast-service';
import { CareerService } from 'src/app/services/career.service';

@Component({
  selector: 'app-add-career',
  templateUrl: './add-career.component.html',
  styleUrls: ['./add-career.component.scss']
})
export class AddCareerComponent implements OnInit {

  private career = {}

  constructor(
    private toastCtrl: ToastService,
    private careerService: CareerService,
    private location: Location
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.careerService.saveCareer(this.career).subscribe(
      () => {
        this.toastCtrl.presentToast('Has Agregado una Carrera!');
        this.location.back();
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Ha Ocurrido un Error :(');
      }
    )
  }
}