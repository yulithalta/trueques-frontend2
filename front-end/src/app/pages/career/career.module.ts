import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../components/shared.module';

import { CareerComponent } from './career.component';
import { AddCareerComponent } from './add-career/add-career.component';
import { UpdateCareerComponent } from './update-career/update-career.component';


@NgModule({
  declarations: [CareerComponent, AddCareerComponent, UpdateCareerComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: CareerComponent
      },
      {
        path: 'agregar-carrera',
        component: AddCareerComponent
      },
      {
        path: 'modificar-carrera',
        component: UpdateCareerComponent
      }
    ])
  ]
})
export class CareerModule { }
