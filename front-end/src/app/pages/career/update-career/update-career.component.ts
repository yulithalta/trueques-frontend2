import { Component, OnInit} from '@angular/core';
import { CareerService} from '../../../services/career.service';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/services/toast-service';
@Component({
  selector: 'app-update-career',
  templateUrl: './update-career.component.html',
  styleUrls: ['./update-career.component.scss']
})
export class UpdateCareerComponent implements OnInit {
  public careerReference;
  public career = {
    "name": '',
  }
  constructor(
    private careerService: CareerService,
    private toastCtrl: ToastService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.retrieveCareer()
  }

  retrieveCareer(){
    this.careerReference = JSON.parse(localStorage.getItem("career"));
    this.careerReference.title = this.careerReference.title.split(": ")
    this.careerReference.title = this.careerReference.title[1]
    console.log("el career: " + this.careerReference);
  }

  onUpdate(){
    this.career.name = this.careerReference.title
    console.log(this.career)
    this.careerService.updateCareer(this.careerReference.id, this.career).subscribe(
      () => {
        this.toastCtrl.presentToast('Has Modificado la carrera!');
        this.location.back();
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Ha Ocurrido un Error :(');
      }
    )
  }

}
