import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UniversitiesService } from '../../services/universities.service';

import { ToastService } from 'src/app/services/toast-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-universities',
  templateUrl: './universities.component.html',
  styleUrls: ['./universities.component.scss']
})
export class UniversitiesComponent implements OnInit {
  public universities;
  
  constructor(
    private universitiesService: UniversitiesService,
    private toastCtrl: ToastService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getUniversities();
  }

  // events
  onItemClick(params): void {
    let universities = JSON.stringify(params)
    this.toastCtrl.presentToast('onItemClick: '+ universities);
  }

  
  getUniversities(){
    this.universitiesService.getUniversities().subscribe(universities => {
      this.universities = universities
    })
  }
  
  onDelete(universities){
    this.universitiesService.deleteUniversities(universities.id).subscribe(
      () => {
        this.toastCtrl.presentToast('Se ha eliminado la universidad ' + universities.title);
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Error: ' + error.statusText);
      }
    );
  }

  //save the dog to update in localStorage, then is retrieved in the update-dog.component.ts
  onUpdate(universities){
    localStorage.setItem("universities", JSON.stringify(universities));
    this.router.navigate(['/universities/modificar-carrera']);
  }
  
}
