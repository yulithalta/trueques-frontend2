import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../components/shared.module';

import { UniversitiesComponent } from './universities.component';
import { AddUniversitiesComponent } from './add-universities/add-universities.component';
import { UpdateUniversitiesComponent } from './update-universities/update-universities.component';


@NgModule({
  declarations: [UniversitiesComponent, AddUniversitiesComponent, UpdateUniversitiesComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: UniversitiesComponent
      },
      {
        path: 'agregar-universidad',
        component: AddUniversitiesComponent
      },
      {
        path: 'modificar-universidad',
        component: UpdateUniversitiesComponent
      }
    ])
  ]
})
export class UniversitiesModule { }
