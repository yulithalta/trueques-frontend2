import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUniversitiesComponent } from './update-universities.component';

describe('UpdateUniversitiesComponent', () => {
  let component: UpdateUniversitiesComponent;
  let fixture: ComponentFixture<UpdateUniversitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUniversitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUniversitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
