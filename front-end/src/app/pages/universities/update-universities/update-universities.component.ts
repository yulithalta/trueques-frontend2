import { Component, OnInit} from '@angular/core';
import { UniversitiesService} from '../../../services/universities.service';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/services/toast-service';
@Component({
  selector: 'app-update-universities',
  templateUrl: './update-universities.component.html',
  styleUrls: ['./update-universities.component.scss']
})
export class UpdateUniversitiesComponent implements OnInit {
  public universitiesReference;
  public universities = {
    "name": '',
  }
  constructor(
    private universitiesService: UniversitiesService,
    private toastCtrl: ToastService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.retrieveUniversities()
  }

  retrieveUniversities(){
    this.universitiesReference = JSON.parse(localStorage.getItem("universities"));
    this.universitiesReference.title = this.universitiesReference.title.split(": ")
    this.universitiesReference.title = this.universitiesReference.title[1]
    console.log("el universities: " + this.universitiesReference);
  }

  onUpdate(){
    this.universities.name = this.universitiesReference.title
    console.log(this.universities)
    this.universitiesService.updateUniversities(this.universitiesReference.id, this.universities).subscribe(
      () => {
        this.toastCtrl.presentToast('Has Modificado la universidad!');
        this.location.back();
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Ha Ocurrido un Error :(');
      }
    )
  }

}
