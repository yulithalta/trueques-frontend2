import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/services/toast-service';
import { UniversitiesService } from 'src/app/services/universities.service';

@Component({
  selector: 'app-add-universities',
  templateUrl: './add-universities.component.html',
  styleUrls: ['./add-universities.component.scss']
})
export class AddUniversitiesComponent implements OnInit {

  private universities = {}

  constructor(
    private toastCtrl: ToastService,
    private universitiesService: UniversitiesService,
    private location: Location
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.universitiesService.saveUniversities(this.universities).subscribe(
      () => {
        this.toastCtrl.presentToast('Has Agregado una Universidad');
        this.location.back();
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Ha Ocurrido un Error :(');
      }
    )
  }
}