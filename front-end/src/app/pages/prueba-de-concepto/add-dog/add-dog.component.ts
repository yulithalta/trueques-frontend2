import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/services/toast-service';
import { DogService } from 'src/app/services/dog.service';

@Component({
  selector: 'app-add-dog',
  templateUrl: './add-dog.component.html',
  styleUrls: ['./add-dog.component.scss']
})
export class AddDogComponent implements OnInit {

  private dog = {}

  constructor(
    private toastCtrl: ToastService,
    private dogService: DogService,
    private location: Location
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.dogService.saveDog(this.dog).subscribe(
      () => {
        this.toastCtrl.presentToast('Has Agregado una Mascota!');
        this.location.back();
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Ha Ocurrido un Error :(');
      }
    )
  }
}