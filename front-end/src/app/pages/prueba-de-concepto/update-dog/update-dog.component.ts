import { Component, OnInit} from '@angular/core';
import { DogService} from '../../../services/dog.service';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/services/toast-service';
@Component({
  selector: 'app-update-dog',
  templateUrl: './update-dog.component.html',
  styleUrls: ['./update-dog.component.scss']
})
export class UpdateDogComponent implements OnInit {
  public dogReference;
  public dog = {
    "name": '',
    "breed": '',
    "age": '',
  }
  constructor(
    private dogService: DogService,
    private toastCtrl: ToastService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.retrieveDog()
  }

  retrieveDog(){
    this.dogReference = JSON.parse(localStorage.getItem("dog"));
    this.dogReference.title = this.dogReference.title.split(": ")
    this.dogReference.title = this.dogReference.title[1]
    this.dogReference.description = this.dogReference.description.split(": ")
    this.dogReference.description = this.dogReference.description[1]
    this.dogReference.shortDescription = this.dogReference.shortDescription.split(": ")
    this.dogReference.shortDescription = this.dogReference.shortDescription[1]
    console.log("el dog: " + this.dogReference);
  }

  onUpdate(){
    this.dog.name = this.dogReference.title
    this.dog.breed = this.dogReference.description
    this.dog.age = this.dogReference.shortDescription
    console.log(this.dog)
    this.dogService.updateDog(this.dogReference.id, this.dog).subscribe(
      () => {
        this.toastCtrl.presentToast('Has Modificado la mascota!');
        this.location.back();
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Ha Ocurrido un Error :(');
      }
    )
  }

}
