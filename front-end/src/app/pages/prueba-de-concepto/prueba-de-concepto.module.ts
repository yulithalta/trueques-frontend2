import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from './../../components/shared.module';

import { PruebaDeConceptoComponent } from './prueba-de-concepto.component';
import { AddDogComponent } from './add-dog/add-dog.component';
import { UpdateDogComponent } from './update-dog/update-dog.component';


@NgModule({
  declarations: [PruebaDeConceptoComponent, AddDogComponent, UpdateDogComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PruebaDeConceptoComponent
      },
      {
        path: 'agregar-mascota',
        component: AddDogComponent
      },
      {
        path: 'modificar-mascota',
        component: UpdateDogComponent
      }
    ])
  ]
})
export class PruebaDeConceptoModule { }
