import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DogService } from '../../services/dog.service';

import { ToastService } from 'src/app/services/toast-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-prueba-de-concepto',
  templateUrl: './prueba-de-concepto.component.html',
  styleUrls: ['./prueba-de-concepto.component.scss']
})
export class PruebaDeConceptoComponent implements OnInit {
  public dogs;
  
  constructor(
    private dogService: DogService,
    private toastCtrl: ToastService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getDogs()
  }

  // events
  onItemClick(params): void {
    let dog = JSON.stringify(params)
    this.toastCtrl.presentToast('onItemClick: '+ dog);
  }

  
  getDogs(){
    this.dogService.getDogs().subscribe(dogs => {
      this.dogs = dogs
    })
  }
  
  onDelete(dog){
    this.dogService.deleteDog(dog.id).subscribe(
      () => {
        this.toastCtrl.presentToast('Se ha eliminado el perro ' + dog.title);
      },
      (error) => {
        console.log(error)
        this.toastCtrl.presentToast('Error: ' + error.statusText);
      }
    );
  }

  //save the dog to update in localStorage, then is retrieved in the update-dog.component.ts
  onUpdate(dog){
    localStorage.setItem("dog", JSON.stringify(dog));
    this.router.navigate(['/pdc/modificar-mascota']);
  }
  
}
