import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebaDeConceptoComponent } from './prueba-de-concepto.component';

describe('PruebaDeConceptoComponent', () => {
  let component: PruebaDeConceptoComponent;
  let fixture: ComponentFixture<PruebaDeConceptoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PruebaDeConceptoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebaDeConceptoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
