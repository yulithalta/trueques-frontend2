import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Universities } from '../models/universities.model';

@Injectable({
  providedIn: 'root'
})
export class UniversitiesService {

  constructor(
    private http: HttpClient
  ) { }

  getUniversities() {
    return this.http.get('http://localhost:3333/universities').pipe(
      map((universities:any[]) => {
        console.log(`universities = ${JSON.stringify(universities,undefined,2)}`)
        let data = {
          items: []
        }
        universities.forEach(universities => {
          data.items.push({
            id: `${universities.id}`,
            title: `Nombre: ${universities.name}`,
            longDescription: '',
            image: 'assets/imgs/avatar/0.jpg'
          })
        });
        console.log(`data = ${JSON.stringify(data,undefined,2)}`)
        return data
      })
    )
  }

  deleteUniversities(id){
    return this.http.delete('http://localhost:3333/universities/'+id);
  }


  saveUniversities(universities) {
    return this.http.post('http://localhost:3333/universities', universities);
  }

  updateUniversities(id, universities){
    return this.http.put('http://localhost:3333/universities/'+id, universities); 
  }
}
