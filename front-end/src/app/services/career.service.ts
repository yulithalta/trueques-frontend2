import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CareerService {

  constructor(
    private http: HttpClient
  ) { }

  getCareer() {
    return this.http.get('http://localhost:3333/career').pipe(
      map((career:any[]) => {
        console.log(`career = ${JSON.stringify(career,undefined,2)}`)
        let data = {
          items: []
        }
        career.forEach(career => {
          data.items.push({
            id: `${career.id}`,
            title: `Nombre: ${career.name}`,
          })
        });
        console.log(`data = ${JSON.stringify(data,undefined,2)}`)
        return data
      })
    )
  }

  deleteCareer(id){
    return this.http.delete('http://localhost:3333/career/'+id);
  }


  saveCareer(career) {
    return this.http.post('http://localhost:3333/career', career);
  }

  updateCareer(id, career){
    return this.http.put('http://localhost:3333/career/'+id, career); 
  }
}
