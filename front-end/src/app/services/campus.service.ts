import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CampusService {

  constructor(
    private http: HttpClient
  ) { }

  getCampus() {
    return this.http.get('http://localhost:3333/campuses').pipe(
      map((campus:any[]) => {
        console.log(`campus = ${JSON.stringify(campus,undefined,2)}`)
        let data = {
          items: []
        }
        campus.forEach(campus => {
          data.items.push({
            id: `${campus.id}`,
            university_id: `${campus.university_id}`,
            name: `${campus.name}`,
            address: `${campus.address}`,
            longDescription: '',
            image: 'assets/imgs/avatar/0.jpg'
          })
        });
        console.log(`data = ${JSON.stringify(data,undefined,2)}`)
        return data
      })
    )
  }

  deleteCampus(id){
    return this.http.delete('http://localhost:3333/campuses/'+id);
  }


  saveCampus(campus) {
    return this.http.post('http://localhost:3333/campuses', campus);
  }

  updateCampus(id, campus){
    return this.http.put('http://localhost:3333/campuses/'+id, campus); 
  }
}
