import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DogService {
  constructor(
    private http: HttpClient
  ) { }

  getDogs() {
    return this.http.get('http://localhost:3333/dogs').pipe(
      map((dogs:any[]) => {
        console.log(`dogs = ${JSON.stringify(dogs,undefined,2)}`)
        let data = {
          items: []
        }
        dogs.forEach(dog => {
          data.items.push({
            id: `${dog.id}`,
            title: `Nombre: ${dog.name}`,
            description: `Raza: ${dog.breed}`,
            shortDescription: `Edad: ${dog.age}`,
            longDescription: '',
            image: 'assets/imgs/avatar/0.jpg'
          })
        });
        console.log(`data = ${JSON.stringify(data,undefined,2)}`)
        return data
      })
    )
  }

  deleteDog(id){
    return this.http.delete('http://localhost:3333/dogs/'+id);
  }


  saveDog(dog) {
    return this.http.post('http://localhost:3333/dogs', dog);
  }

  updateDog(id, dog){
    return this.http.put('http://localhost:3333/dogs/'+id, dog); 
  }
}