import { TestBed } from '@angular/core/testing';

import { CampusitemService } from './campusitem.service';

describe('CampusitemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CampusitemService = TestBed.get(CampusitemService);
    expect(service).toBeTruthy();
  });
});
