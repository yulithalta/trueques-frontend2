import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(
    private http: HttpClient
  ) { }

  getCategory() {
    return this.http.get('http://localhost:3333/category').pipe(
      map((category:any[]) => {
        console.log(`category = ${JSON.stringify(category,undefined,2)}`)
        let data = {
          items: []
        }
        category.forEach(category => {
          data.items.push({
            id: `${category.id}`,
            title: `Nombre: ${category.name}`,
            longDescription: '',
            image: 'assets/imgs/avatar/0.jpg'
          })
        });
        console.log(`data = ${JSON.stringify(data,undefined,2)}`)
        return data
      })
    )
  }

  deleteCategory(id){
    return this.http.delete('http://localhost:3333/category/'+id);
  }


  saveCategory(category) {
    return this.http.post('http://localhost:3333/category', category);
  }

  updateCategory(id, category){
    return this.http.put('http://localhost:3333/category/'+id, category); 
  }
}
