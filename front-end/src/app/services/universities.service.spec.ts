import { TestBed } from '@angular/core/testing';

import { UniversitiesService } from './universities.service';

describe('UniversitiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UniversitiesService = TestBed.get(UniversitiesService);
    expect(service).toBeTruthy();
  });
});
