import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleCardListComponent } from './google-card-list.component';

describe('GoogleCardListComponent', () => {
  let component: GoogleCardListComponent;
  let fixture: ComponentFixture<GoogleCardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleCardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
