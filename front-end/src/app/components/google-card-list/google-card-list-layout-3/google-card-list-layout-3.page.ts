import { Component, Output, EventEmitter, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'cs-google-card-list-layout-3',
  templateUrl: 'google-card-list-layout-3.page.html',
  styleUrls: ['google-card-list-layout-3.page.scss'],
})
export class GoogleCardListLayout3Page implements OnChanges {
  @Input() data: any;

  @Output() onItemClick = new EventEmitter();
  @Output() onExplore = new EventEmitter();
  @Output() onShare = new EventEmitter();
  @Output() onDelete = new EventEmitter();
  @Output() onUpdate = new EventEmitter();

  ngOnChanges(changes: { [propKey: string]: any }) {
    this.data = changes['data'].currentValue;
  }

  onItemClickFunc(item) {
    if (event) {
      event.stopPropagation();
    }
    this.onItemClick.emit(item);
  }

  onShareFunc(item) {
    if (event) {
      event.stopPropagation();
    }
    this.onShare.emit(item);
  }

  onDeleteFunc(item) {
    if (event) {
      event.stopPropagation();
    }
    this.onDelete.emit(item);
  }

  onUpdateFunc(item) {
    if (event) {
      event.stopPropagation();
    }
    this.onUpdate.emit(item);
  }

  
}
