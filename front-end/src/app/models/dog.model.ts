export class Dog{
    constructor(
        public name: string,
        public owner: number,
        public breed: string,
        public age: number,
    ){}
}