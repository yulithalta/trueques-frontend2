import { Binary } from "@angular/compiler";

export class Items{
    constructor(
        public user_id: number,
        public name: string,
        public desc: string,
        public image: Binary,
        public wantend: string,
    ){}
}