export class Campus{
    constructor(
        public university_id: number,
        public name: string,
        public address: string,
    ){}
}