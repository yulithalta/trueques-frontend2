import { Binary } from "@angular/compiler";

export class UserDetails{
    constructor(
        public id_user: number,
        public id_career: number,
        public id_campus: number,
        public name: string,
        public last: string,
        public image:Binary,
        public email:string,
        public password: string
    ){}
}